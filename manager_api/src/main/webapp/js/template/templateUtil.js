//判断等于null
function isNull(data) {
	return data == "" || data == undefined || data == null || $.trim(data).length == 0;  
}
//判断不等于null
function isNotNull(data) {  
	return !isNull(data);  
}
//data 数据  str目标值
function strIndexOf(data,str){
	return isNotNull(data)&&isNotNull(str)&&data.indexOf(str)>=0;
}
//这个方法 用了$.trim()会把data去掉左右空格 在做处理
//固定长度  多出部分 用 符号代替
/**
 * data:数据
 len:长度
 symbol:替换的字符串
 minusLen:截取字符串的时候  会执行  $.trim(data).substring(0,len-minusLen)  默认值是 0
 */
function fixed_lengt_symbol_replace(data,len,symbol,minusLen){
	var dataNew = "";
	if(isNull(minusLen)){
		minusLen = 0;
	}
	if(isNotNull(data)){
		if($.trim(data).length>len){
			//当data 不为null 并且 data长度 大于len的时候 开始截取字符串
			dataNew = $.trim(data).substring(0,len-minusLen)+symbol;
		}else{
			dataNew = data;
		}
	}
	return dataNew;
}
//判断  如果是 null  就返回默认值 
//如果不为null 返回 原来的值
/**
 * dd
 */
function isNullGetDef(data,def){
	if(isNull(data)){
		return def;
	}
	return data;
}
template.helper("isNull", isNull);
template.helper("isNotNull",isNotNull);
template.helper("strIndexOf", strIndexOf);
template.helper("fixedLengtSymbolReplace",fixed_lengt_symbol_replace);
template.helper("isNullGetDef", isNullGetDef);

//自己封装的方法 
//用 artTemplate 修改页面上的值  ajax
/* ajaxData ajax的配置
 * templateId 模板id
 * pageHtmlId 页面上html的id
 * */
//参考
//updataPageHtml({
//	type:"post",
//	url:"${basePath}/cms/articleList",
//	data:{pageNo:1,module:"article",pageSize:5},
//	dataType:"json"
//},'t_cms_list','cms_list');

function updataPageHtml(ajaxData,templateId,pageHtmlId){
	$.ajax({
	    type : isNullGetDef(ajaxData.type,"post"),  
	    url : ajaxData.url,
	    data : isNullGetDef(ajaxData.data,{}),
	    dataType : isNullGetDef(ajaxData.dataType,"json"),
	    success : function(data){ 
	    	if(data.succ){
//	    		var html = template(templateId, data);
//	    		$("#"+pageHtmlId).html(html);
	    		updatePageHtmlNotAjax(templateId, pageHtmlId,data)
	    	}
	    },
	    error : function(data){
	    	
	    }
	});  
}
function updatePageHtmlNotAjax(templateId,pageHtmlId,data){
	var html = template(templateId, data);
	$("#"+pageHtmlId).html(html);
	return html;
}