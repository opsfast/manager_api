package com.api.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.api.constant.BaseConstant;
import com.jfinal.handler.Handler;

public class BaseHandler extends Handler{

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		request.setAttribute(BaseConstant.BASE_PATH, request.getContextPath());
		next.handle(target, request, response, isHandled);
	}

}
