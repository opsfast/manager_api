package com.api.controller;

import java.util.Map;

import com.api.model.Project;
import com.api.model.SysConfig;
import com.api.util.DesUtil;
import com.api.util.GsonUtil;
import com.google.gson.reflect.TypeToken;
import com.jfinal.kit.StrKit;

public class IndexController extends BaseController{
	
	public void index(){
		render("project/projectList.html");
	}
	public void home(){
		render("home.html");
	}
	public void indexHome(){
		setAttr("project_id", getPara("project_id"));
		setAttr("project", "");
		if(isNullParams("project_id")){
			Project  project = Project.dao.findById(getPara("project_id"));
			if(project!=null){
				setAttr("project", project);
				String projectName = project.getProjectName();
				if(StrKit.notBlank(projectName)){
					String tempProjectName = "";
					if(projectName.length()>10){
						tempProjectName = projectName.substring(0, 10)+"..";
					}else{
						tempProjectName = projectName;
					}
					setAttr("hideProjectName", tempProjectName);
				}
			}
		}
		
		
		SysConfig sysConfig = SysConfig.dao.findFirst("select * from sys_config where name=? limit 1","is_pay");
		if(sysConfig!=null){
			setAttr("isPay", sysConfig.getVal());
		}
		render("index.html");
	}
	//方便des接口测试的
	//解密之后   再把得到的数据加密返还回去
	@SuppressWarnings("rawtypes")
	public void returnDes(){
		String returnData = "hello world";
		if(isNullParams("data")){
			String data_temp = getPara("data");
			String data = DesUtil.decode(data_temp);
			Map map = (Map) GsonUtil.fromJsonToObject(data, new TypeToken<Map<String, String>>() {}.getType());
			returnData = DesUtil.encode(GsonUtil.toJson(map));
		}
		renderJson(returnData);
	}
}
