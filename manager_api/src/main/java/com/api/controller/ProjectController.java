package com.api.controller;

import java.util.ArrayList;
import java.util.List;

import com.api.model.Domain;
import com.api.model.Project;
import com.api.util.ApiJson;
import com.jfinal.kit.StrKit;

public class ProjectController extends BaseController {
	public void index(){
//		setAttr("project_id", getPara("project_id"));
		render("projectList.html");
	}
	public void projectList(){
		render("projectList.html");
	}
	public void getProjectLists(){
		ApiJson apiJson = new ApiJson();
		String sql="select * from project where status = 1 order by id desc";
		List<Project> projectList=Project.dao.find(sql);
		apiJson.setIsSuccess(true);
		apiJson.setCode("0");
		apiJson.setMessage("请求成功");
		apiJson.setData(projectList);
		renderJson(apiJson);
	}
	public void delProject(){
		ApiJson apiJson = new ApiJson();
		apiJson.setMessage("删除成功");
		if(isNullParams("projectId")){
			Project project = Project.dao.findById(getPara("projectId"));
			if(project!=null){
				project.setStatus(0);
				project.update();
			}
		}
		renderJson(apiJson);
	}
	public void addProjectPage(){
		render("addProject.html");
	}
	
	
	
	
	
	
	public void saveOrEditProject(){
		if(StrKit.notBlank(getPara("projectId"))){
			setAttr("project", Project.dao.findById(getPara("projectId")));
		}else{
			setAttr("project", "");
		}
		render("saveOrEdit.html");
	}
	public void doSaveOrEdit(){
		System.out.println(getBean(Project.class));
		System.out.println(getModel(Project.class));
		ApiJson apiJson = new ApiJson();
		apiJson.setMessage("请求成功");
		Project project = getModel(Project.class);
		
		if(project.getId()!=null){
			project.update();
		}else{
			project.setStatus(1);
			project.save();
		}
		
		renderJson(apiJson);
	}
	
	
	
	
	
	
	
	
	
	public void addOrUpdateProject(){
		ApiJson apiJson = new ApiJson();
		boolean istrue=false;
		Project p= getBean(Project.class);
		if(p.getId()==null)
			istrue=p.save();
		else
			istrue=p.update();
		apiJson.setIsSuccess(istrue);
		if(istrue){
			apiJson.setCode("0");
			apiJson.setMessage("请求成功");
		}else{
			apiJson.setCode("-2");
			apiJson.setMessage("请求失败");
		}
		renderJson(apiJson);
	}
	public void saveDomain(){
		setAttr("project_id", getPara("project_id"));
		render("saveDomain.html");
	}
	public void selectDelDomain(){
		setAttr("project_id", getPara("project_id"));
		List<Domain> domainList = Domain.dao.find("select * from domain where project_id=? order by id asc", getPara("project_id"));
		setAttr("domainList", domainList);
		render("selectDelDomain.html");
	}
	public void doSaveDomain(){
		ApiJson apiJson = new ApiJson();
		boolean istrue=false;
		Domain data= getBean(Domain.class);
		istrue=data.save();
		apiJson.setIsSuccess(istrue);
		if(istrue){
			apiJson.setCode("0");
			apiJson.setMessage("请求成功");
		}else{
			apiJson.setCode("-2");
			apiJson.setMessage("请求失败");
		}
		renderJson(apiJson);
	}
	public void delDomain(){
		ApiJson apiJson = new ApiJson();
		boolean istrue=false;
		Domain domain = Domain.dao.findById(getPara("domainId"));
		if(domain==null){
			istrue = true;
		}else{
			istrue = domain.delete();
		}
		apiJson.setIsSuccess(istrue);
		if(istrue){
			apiJson.setCode("0");
			apiJson.setMessage("请求成功");
		}else{
			apiJson.setCode("-2");
			apiJson.setMessage("请求失败");
		}
		renderJson(apiJson);
	}
}
