package com.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import com.api.dispose.base.BaseDispose;
import com.api.dispose.base.DesDispose;
import com.api.model.Api;
import com.api.model.Domain;
import com.api.model.Module;
import com.api.model.Project;
import com.api.model.Requestheader;
import com.api.model.Requestparams;
import com.api.model.Responseparams;
import com.api.util.ApiJson;
import com.api.util.ApiUtil;
import com.api.util.FormatUtil;
import com.api.util.GsonUtil;
import com.api.util.HttpJson;
import com.jfinal.aop.Before;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.plugin.ehcache.CacheKit;
	
public class ApiController extends BaseController{
	public Object lock = new Object();
	public void index(){
		Api apiInfo = Api.dao.findById(getPara("apiId"));
		Module moduleInfo = Module.dao.findById(apiInfo.getModuleId());
		Project projectInfo = Project.dao.findById(moduleInfo.getProjectId());
		setAttr("apiInfo", apiInfo);
		setAttr("moduleInfo", moduleInfo);
		setAttr("projectInfo", projectInfo);
		
		List<Domain> domainList = Domain.dao.find("select * from domain where project_id=? order by id asc", projectInfo.getId());
		
		Domain damainDef = new Domain();
		damainDef.setIpOrDomain(apiInfo.getIp());
		damainDef.setPort(apiInfo.getPort());
		damainDef.setProjectId(projectInfo.getId());
		damainDef.setName("默认");
		damainDef.setId(-1);
		if(domainList==null){
			domainList = new ArrayList<Domain>();
		}
		domainList.add(0, damainDef);
		setAttr("domainList", domainList);
		
		List<Requestparams> requestParamsList = Requestparams.dao.find("select * from requestparams where api_id = ?",getPara("apiId"));
		List<Responseparams> responseParamsList = Responseparams.dao.find("select * from responseparams where api_id = ?",getPara("apiId"));
		List<Requestheader> requestHeaderList = Requestheader.dao.find("select * from requestheader where api_id = ?",getPara("apiId"));
		
		setAttr("requestParamsList", requestParamsList);
		if(requestParamsList!=null&&requestParamsList.size()>0){
			String requestParamsListIds = "";
			for (int i = 0; i < requestParamsList.size(); i++) {
				if(StrKit.notBlank(requestParamsListIds)){
					requestParamsListIds = requestParamsListIds + "_";
				}
				requestParamsListIds = requestParamsListIds + requestParamsList.get(i).getId().toString();
			}
			setAttr("requestParamsListIds", requestParamsListIds);
		}
		
		setAttr("requestHeaderList", requestHeaderList);
		if(requestHeaderList!=null&&requestHeaderList.size()>0){
			String requestHeaderListIds = "";
			for (int i = 0; i < requestHeaderList.size(); i++) {
				if(StrKit.notBlank(requestHeaderListIds)){
					requestHeaderListIds = requestHeaderListIds + "_";
				}
				requestHeaderListIds = requestHeaderListIds + requestHeaderList.get(i).getId().toString();
			}
			setAttr("requestHeaderListIds", requestHeaderListIds);
		}
		
		setAttr("responseParamsList", responseParamsList);
		if(responseParamsList!=null&&responseParamsList.size()>0){
			String responseParamsListIds = "";
			for (int i = 0; i < responseParamsList.size(); i++) {
				if(StrKit.notBlank(responseParamsListIds)){
					responseParamsListIds = responseParamsListIds + "_";
				}
				responseParamsListIds = responseParamsListIds + responseParamsList.get(i).getId().toString();
			}
			setAttr("responseParamsListIds", responseParamsListIds);
		}
		render("index.html");
	}
	public void doRequest(){
		ApiJson json = new ApiJson();
		String allUrl = ApiUtil.getAllUrl(getRequest());
		//参数验证
		if(ApiUtil.isBank_allUrl(getRequest())){
			json.setIsSuccess(false);
			json.setMessage("ip url 不能为空");
			renderJson(json);
			return;
		}
		BaseDispose baseDispose =  getDispose();
		Map<String, String> queryParas = baseDispose.disposeRequest(getManagerApi_requestParams());
		HttpJson httpJson = ApiUtil.httpRequest(allUrl, queryParas, getPara("requestMethod"),getManagerApi_requestHeader());
		if(!httpJson.getIsSuccess()){
			json.setIsSuccess(false);
			json.setMessage("错误信息是："+httpJson.getData());
			renderJson(json);
			return;
		}
		//处理返回值
		String responseStr = baseDispose.disposeResponse(httpJson.getData());
		json.setData(responseStr);
		json.setIsSuccess(true);
		json.setMessage("请求成功");
		renderJson(json);
	}
	public void doBatchRequest(){
		final ApiJson json = new ApiJson();
		final String allUrl = ApiUtil.getAllUrl(getRequest());
		//参数验证
		if(ApiUtil.isBank_allUrl(getRequest())){
			json.setIsSuccess(false);
			json.setMessage("ip url 不能为空");
			renderJson(json);
			return;
		}
		final BaseDispose baseDispose = getDispose();
		final Map<String, String> queryParas = baseDispose.disposeRequest(getManagerApi_requestParams());
		final String requestMethod = getPara("requestMethod");
		CacheKit.remove("apiResponseList", getPara("apiId"));
		int threadNum = getParaToInt("threadNum", 1);
		final CountDownLatch latch=new CountDownLatch(threadNum);
		final int requestNum = getParaToInt("requestNum", 1);
		for (int i = 0; i < threadNum; i++) {
			new Thread(new Runnable() {
				public void run() {
					for (int j = 0; j < requestNum; j++) {
						HttpJson httpJson = ApiUtil.httpRequest(allUrl, queryParas, requestMethod,getManagerApi_requestHeader());
						if(!httpJson.getIsSuccess()){
							json.setIsSuccess(false);
							json.setMessage("错误信息是："+httpJson.getData());
							renderJson(json);
							return;
						}
						//处理返回值
						String responseStr = baseDispose.disposeResponse(httpJson.getData());
//						synchronized (lock) {
							List<String> list = CacheKit.get("apiResponseList", getPara("apiId"));
							if(list == null){
								list = new ArrayList<String>();
							}
							list.add(responseStr);
							CacheKit.put("apiResponseList", getPara("apiId"),list);
//						}
						LogKit.info("请求完了");
					}
					latch.countDown();
				}
			}).start();
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
			LogKit.error("批处理 线程{CountDownLatch }  报异常"+e);
		}
		LogKit.info("走完了");
		List<String> list = CacheKit.get("apiResponseList", getPara("apiId"));
		
		json.setData(GsonUtil.toJson(list));
		json.setIsSuccess(true);
		json.setMessage("请求成功");
		renderJson(json);
	}
	
	private BaseDispose getDispose() {
		BaseDispose baseDispose;
		if("base".equals(getPara("disposeType"))){
			baseDispose = new BaseDispose();
		}else if("des".equals(getPara("disposeType"))){
			 baseDispose = new DesDispose();
		}else{
			baseDispose = new BaseDispose();
		}
		return baseDispose;
	}
	
	@Before(Tx.class)
	public void saveApiInfo(){
		//存储api数据
		ApiJson jsonApi = new ApiJson();
		jsonApi.setMessage("保存成功");
		Api api= getApi();
		if(api.getId()==null){
			api.save();
		}else{
			api.update();
		}
		//暴力 删除数据库 requestParams 数据
		if(api.getId()!=null){
			Db.update("delete from requestparams where api_id=?", api.getId());
			Db.update("delete from responseparams where api_id=?", api.getId());
			Db.update("delete from requestheader where api_id=?", api.getId());
		}
		//存储requestHeader
		List<Requestheader> requestHeaderList = getManagerApi_requestHeader();
		if(requestHeaderList!=null&&requestHeaderList.size()>0){
			for (int i = 0; i < requestHeaderList.size(); i++) {
				Requestheader requestheader = requestHeaderList.get(i);
				if(api.getId()!=null){
					requestheader.setApiId(api.getId());
				}
				requestheader.save();
			}
		}
		//存储requestParams
		List<Requestparams> requestParamsList = getManagerApi_requestParams();
		if(requestParamsList!=null&&requestParamsList.size()>0){
			for (int i = 0; i < requestParamsList.size(); i++) {
				Requestparams requestparams = requestParamsList.get(i);
				if(api.getId()!=null){
					requestparams.setApiId(api.getId());
				}
				requestparams.save();
			}
		}
		//存储responseParams
		List<Responseparams> responseParamsList = getManagerApi_responseParams();
		if(responseParamsList!=null&&responseParamsList.size()>0){
			for (int i = 0; i < responseParamsList.size(); i++) {
				Responseparams responseparams = responseParamsList.get(i);
				if(api.getId()!=null){
					responseparams.setApiId(api.getId());
				}
				responseparams.save();
			}
		}
		renderJson(jsonApi);
	}
	public void formatStr(){
		ApiJson json = new ApiJson();
		json.setMessage("格式化成功");
		String data = getPara("data");
		if(StrKit.notBlank(data)){
			json.setData(FormatUtil.formatJson(data));
		}else{
			json.setIsSuccess(false);
		}
		renderJson(json);
	}
	public void saveOrEdit(){
		List<Module> list = Module.dao.find("select * from module where status = 1 and project_id=? order by orderNum desc",getPara("project_id"));
		if(list!=null&&list.size()>0){
			setAttr("moduleList", list);
		}
		setAttr("apiInfo", new Object());
		if(isNullParams("api_id")){
			Api apiInfo= Api.dao.findById(getPara("api_id"));
			if(apiInfo!=null){
				setAttr("apiInfo", apiInfo);
			}
			
		}
		render("saveOrEdit.html");
	}
	public void doSaveOrEdit(){
		ApiJson apiJson = new ApiJson();
		apiJson.setMessage("请求成功");
		Api api = new Api();
		if(isNullParams("api_id")){
			api = Api.dao.findById(getPara("api_id"));
		}
		
		api.setName(getPara("name"));
		api.setModuleId(getParaToInt("moduleId"));
		
		if(isNullParams("api_id")){
			api.update();
		}else{
			api.setStatus(1);
			api.save();
		}
		
		renderJson(apiJson);
	}
	
	public void deleteApi(){
		ApiJson apiJson = new ApiJson();
		if(isParaBlank("api_id")){
			apiJson.setMessage("id不能为空");
			apiJson.setIsSuccess(false);
			renderJson(apiJson);
			return;
		}
		Api api = Api.dao.findById(getPara("api_id"));
		if(api!=null){
			api.setStatus(0);
			api.update();
		}
		apiJson.setMessage("删除成功");
		renderJson(apiJson);
	}
}
