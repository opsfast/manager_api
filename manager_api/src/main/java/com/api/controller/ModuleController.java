package com.api.controller;

import java.util.List;

import com.api.model.Api;
import com.api.model.Module;
import com.api.util.ApiJson;
import com.jfinal.kit.StrKit;
	
public class ModuleController extends BaseController{
	public void getMenusInfo(){
		String projectId = getPara("projectId");
		ApiJson json = new ApiJson();
		if(StrKit.isBlank(projectId)){
			json.setCode("-1");
			json.setIsSuccess(false);
			json.setMessage("project_id不能为空");
			renderJson(json);
			return;
		}
		List<Module> moduleList = Module.dao.find("select * from module where status = 1 and project_id=? order by orderNum desc",projectId);
		if(moduleList!=null&&moduleList.size()>0){
			List<Api> apiList = Api.dao.find("select api.* from api LEFT JOIN module on api.module_id=module.id where module.status = 1 and api.status=1 and module.project_id=?",projectId);
			json.putDataMap("moduleList", moduleList);
			json.putDataMap("apiList", apiList);
		}else{
//			json.putDataMap("moduleList", null);
//			json.putDataMap("apiList", null);
		}
	
		renderJson(json);
	}
	public void saveOrEditModule(){
		setAttr("moduleId", getPara("moduleId"));
		setAttr("project_id", getPara("project_id"));
		if(StrKit.notBlank(getPara("moduleId"))){
			setAttr("module", Module.dao.findById(getPara("moduleId")));
		}else{
			setAttr("module", "");
		}
		render("saveOrEdit.html");
	}
	public void doSaveOrEditModule(){
		ApiJson apiJson = new ApiJson();
		Module module = new Module();
		module.setStatus(getParaToInt("status", 0));
		module.setOrderNum(getParaToInt("orderNum", 0));
		module.setProjectId(getParaToInt("project_id"));
		module.setName(getPara("name"));
		if(StrKit.notBlank(getPara("moduleId"))){
			//edit
			module.setId(getParaToInt("moduleId"));
			module.update();
			apiJson.setMessage("编辑成功");
		}else{
			//save
			module.save();
			apiJson.setMessage("添加成功");
		}
		renderJson(apiJson);
	}
	public void deleteModule(){
		ApiJson apiJson = new ApiJson();
		if(isParaBlank("module_id")){
			apiJson.setMessage("模块id不能为空");
			apiJson.setIsSuccess(false);
			renderJson(apiJson);
			return;
		}
		Module module = Module.dao.findById(getPara("module_id"));
		if(module!=null){
			module.setStatus(0);
			module.update();
		}
		apiJson.setMessage("删除成功");
		renderJson(apiJson);
	}
}
