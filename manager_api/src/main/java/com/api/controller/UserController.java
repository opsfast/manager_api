package com.api.controller;

import javax.servlet.http.HttpSession;

import com.api.config.ApiConstant;
import com.api.model.SysConfig;
import com.api.model.User;
import com.api.util.ApiJson;
import com.api.util.MD5Util;
import com.jfinal.kit.StrKit;
	
public class UserController extends BaseController{
	public void login(){
		render("login.html");
	}
	public void register(){
		render("register.html");
	}
	public void logout(){
		getSession().removeAttribute(ApiConstant.LOGIN_SESSION_NAME);
		redirect("/user/login");
		return;
	}
	public void doLogin(){
		ApiJson json = new ApiJson();
		String name = getPara("name");
		String pwd = getPara("pwd");
		if(!StrKit.notBlank(name)){
			json.setMessage("登陆账号不能为空");
			json.setIsSuccess(false);
			renderJson(json);
			return;
		};
		if(!StrKit.notBlank(pwd)){
			json.setMessage("登陆密码不能为空");
			json.setIsSuccess(false);
			renderJson(json);
			return;
		};
		User user = User.dao.findFirst("select * from user where name = ?",name);
		if(user==null||!user.getPwd().equals(MD5Util.md5(pwd))){
			json.setMessage("账号密码错误");
			json.setIsSuccess(false);
			renderJson(json);
			return;
		}
		HttpSession session = getSession(true);
		//时间设置3天  考虑到我这种每天不关电脑的人 哈哈
		session.setMaxInactiveInterval(60*60*24*3);
		session.setAttribute(ApiConstant.LOGIN_SESSION_NAME, user);
		json.setMessage("登陆成功");
		json.setIsSuccess(true);
		renderJson(json);
	}
	
	public void doRegister(){
		ApiJson json = new ApiJson();
		String name = getPara("name");
		String pwd = getPara("pwd");
		String code = getPara("code");
		if(!StrKit.notBlank(name)){
			json.setMessage("账号不能为空");
			json.setIsSuccess(false);
			renderJson(json);
			return;
		};
		if(!StrKit.notBlank(pwd)){
			json.setMessage("密码不能为空");
			json.setIsSuccess(false);
			renderJson(json);
			return;
		};
		if(!StrKit.notBlank(code)){
			json.setMessage("注册码不能为空");
			json.setIsSuccess(false);
			renderJson(json);
			return;
		};
		SysConfig sysConfig= SysConfig.dao.findFirst("select * from sys_config where name = ?","registerCode");
		if(sysConfig==null || !sysConfig.getVal().equals(code)){
			json.setMessage("注册码不正确");
			json.setIsSuccess(false);
			renderJson(json);
			return;
		};
		User user = User.dao.findFirst("select * from user where name = ?",name);
		if(user!=null){
			json.setMessage("账号重复");
			json.setIsSuccess(false);
			renderJson(json);
			return;
		}
		pwd = MD5Util.md5(pwd);
		user = new User().setName(name).setPwd(pwd);
		user.save();
		json.setMessage("注册成功");
		json.setIsSuccess(true);
		renderJson(json);
	}
}
