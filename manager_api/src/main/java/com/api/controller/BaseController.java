package com.api.controller;

import java.util.ArrayList;
import java.util.List;

import com.api.model.Api;
import com.api.model.Requestheader;
import com.api.model.Requestparams;
import com.api.model.Responseparams;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;

public class BaseController extends Controller{
	/**
	 * 获取页面上 全部参数 封装成List<Requestparams>
	 * @return
	 */
	public List<Requestparams> getManagerApi_requestParams(){
		String requestNames = getPara("requestParams_index_values");
		if(StrKit.isBlank(requestNames)){
			return null;
		}
		String [] requestNamesList = requestNames.split("_");
		List<Requestparams> list = new ArrayList<Requestparams>();
		
		for (int i = 0; i < requestNamesList.length; i++) {
			Requestparams data= new Requestparams();
			String name = getPara("requestParams_"+requestNamesList[i]+"_name");
			data.setName(name);
			
			String val = getPara("requestParams_"+requestNamesList[i]+"_val");
			data.setVal(val);
			String must = getPara("requestParams_"+requestNamesList[i]+"_must","0");
			data.setIsMust(Integer.valueOf(must));
			String notes = getPara("requestParams_"+requestNamesList[i]+"_notes");
			data.setNotes(notes);
			list.add(data);
		}
		return list;
	}
	/**
	 * 获取页面上 全部参数 封装成List<Requestparams>
	 * @return
	 */
	public List<Requestheader> getManagerApi_requestHeader(){
		String requestNames = getPara("requestHeader_index_values");
		if(StrKit.isBlank(requestNames)){
			return null;
		}
		String [] requestNamesList = requestNames.split("_");
		List<Requestheader> list = new ArrayList<Requestheader>();
		
		for (int i = 0; i < requestNamesList.length; i++) {
			Requestheader data= new Requestheader();
			String name = getPara("requestHeader_"+requestNamesList[i]+"_name");
			data.setName(name);
			
			String val = getPara("requestHeader_"+requestNamesList[i]+"_val");
			data.setVal(val);

			String notes = getPara("requestHeader_"+requestNamesList[i]+"_notes");
			data.setNotes(notes);
			list.add(data);
		}
		return list;
	}
	public boolean isNullParams(String name){
		return StrKit.notBlank(getPara(name));
	}
	public Api getApi(){
		Api api = new Api();
		if(isNullParams("apiId")){
			api.setId(getParaToInt("apiId"));
		}
		if(isNullParams("protocol")){
			api.setProtocol(getPara("protocol"));
		}
		if(isNullParams("ip")){
			api.setIp(getPara("ip"));
		}
		if(isNullParams("port")){
			api.setPort(getPara("port"));
		}
		if(isNullParams("url")){
			api.setUrl(getPara("url"));
		}
		if(isNullParams("requestMethod")){
			api.setRequestMethod(getPara("requestMethod"));
		}
		if(isNullParams("requestBody")){
			api.setRequestBody(getPara("requestBody"));
		}
		if(isNullParams("responseBody")){
			api.setResponseBody(getPara("responseBody"));
		}
		if(isNullParams("module_id")){
			api.setModuleId(getParaToInt("module_id"));
		}
		if(isNullParams("disposeType")){
			api.setDisposeType(getPara("disposeType"));
		}
//		api.setCreateTime(createTime);
		return api;
	}
//	public int boolRequestParams(){
//		List<Requestparams> list = getManagerApi_requestParams();
//		if(list==null||list.size()==0){
//			return 1;
//		}
//		for (int i = 0; i < list.size(); i++) {
//			if(StrKit.isBlank(list.get(i).getName())){
//				return 2;
//			}
//			if(Boolean.valueOf(list.get(i).getIsMust().toString()) && StrKit.isBlank(list.get(i).getVal())){
//				return 3;
//			}
//		}
//		return 1;
//	}
	
	public List<Responseparams> getManagerApi_responseParams(){
		String responseNames = getPara("responseParams_index_values");
		if(StrKit.isBlank(responseNames)){
			return null;
		}
		String [] responseNamesList = responseNames.split("_");
		List<Responseparams> list = new ArrayList<Responseparams>();
		
		for (int i = 0; i < responseNamesList.length; i++) {
			Responseparams data= new Responseparams();
			String name = getPara("response_"+responseNamesList[i]+"_name");
			data.setName(name);
			String val = getPara("response_"+responseNamesList[i]+"_val");
			data.setVal(val);
			String notes = getPara("response_"+responseNamesList[i]+"_notes");
			data.setNotes(notes);
			list.add(data);
		}
		return list;
	}
}
