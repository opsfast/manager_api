package com.api.dispose.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.api.model.Requestparams;
import com.api.util.DesUtil;
import com.api.util.GsonUtil;
import com.jfinal.kit.StrKit;

public class DesDispose extends BaseDispose{
	
	/**
	 *  我这边就举个栗子
	 *  每个公司 用到的加密方式都不一样
	 *  继承BaseDispose 就是方便调用 disposeRequest()
	 */
	
	/**
	 *	把参数加密
	 */
	public  Map<String, String> disposeRequest(List<Requestparams> list){
		Map<String, String> temp_map = super.disposeRequest(list);
		Map<String, String> map = new HashMap<String, String>();
		if(map != null){
			map.put("data", DesUtil.encode(GsonUtil.toJson(temp_map)));
		}
		return map;
	}
	//des解密
	public  String disposeResponse(String responseStr){
		if(StrKit.notBlank(responseStr)){
			responseStr = DesUtil.decode(responseStr);
		}
		return responseStr;
	}
}
