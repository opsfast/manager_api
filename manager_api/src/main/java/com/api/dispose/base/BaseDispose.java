package com.api.dispose.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.api.model.Requestparams;

public class BaseDispose {
	/**
	 * 把参数封装成 map
	 * @param list
	 * @return
	 */
	public  Map<String, String> disposeRequest(List<Requestparams> list){
		if(list==null || list.size()<=0){
			return null;
		}
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < list.size(); i++) {
			Requestparams data = list.get(i);
			map.put(data.getName(), data.getVal());
		}
		return map;
	}
	public  String disposeResponse(String responseStr){
		return responseStr;
	}
}
