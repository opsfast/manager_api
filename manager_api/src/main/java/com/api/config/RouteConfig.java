package com.api.config;

import com.api.controller.ApiController;
import com.api.controller.IndexController;
import com.api.controller.ModuleController;
import com.api.controller.ProjectController;
import com.api.controller.UserController;
import com.jfinal.config.Routes;

public class RouteConfig extends Routes{

	@Override
	public void config() {
		setBaseViewPath("/WEB-INF/pages");
		add("index", IndexController.class,"/");
		add("api", ApiController.class,"/api");
		add("project",ProjectController.class,"/project");
		add("module",ModuleController.class,"/module");
		add("user",UserController.class,"/user");
	}
}
