package com.api.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.api.config.ApiConstant;
import com.api.model.User;
import com.api.util.ApiJson;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class LoginInterceptor implements Interceptor{
	public static List<String> notLoginUrlList = new ArrayList<String>();
	static{
		notLoginUrlList.add("/user/login");
		notLoginUrlList.add("/user/register");
		notLoginUrlList.add("/user/doLogin");
		notLoginUrlList.add("/user/doRegister");
		notLoginUrlList.add("/user/logout");
	}
	public void intercept(Invocation inv) {
		;
		if(notLoginUrlList.contains(inv.getActionKey())){
			inv.invoke();
			return;
		}
		User user = (User) inv.getController().getSession().getAttribute(ApiConstant.LOGIN_SESSION_NAME);
		if(user!=null){
			inv.invoke();
		}else{
			if(isAjax(inv.getController().getRequest())){
				ApiJson json = new ApiJson();
				json.setMessage("请登录！");
				json.setIsSuccess(false);
				inv.getController().renderJson(json);
			}else{
				inv.getController().redirect("/user/login");
			}
		}
	}
	public boolean isAjax(HttpServletRequest request){  
	    String header = request.getHeader("X-Requested-With");  
	    boolean isAjax = "XMLHttpRequest".equals(header) ? true:false;  
	    return isAjax;  
	}  
}
