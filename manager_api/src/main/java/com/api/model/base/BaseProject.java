package com.api.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseProject<M extends BaseProject<M>> extends Model<M> implements IBean {

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public M setProjectName(java.lang.String projectName) {
		set("projectName", projectName);
		return (M)this;
	}

	public java.lang.String getProjectName() {
		return get("projectName");
	}

	public M setProtocol(java.lang.String protocol) {
		set("protocol", protocol);
		return (M)this;
	}

	public java.lang.String getProtocol() {
		return get("protocol");
	}

	public M setIp(java.lang.String ip) {
		set("ip", ip);
		return (M)this;
	}

	public java.lang.String getIp() {
		return get("ip");
	}

	public M setPort(java.lang.String port) {
		set("port", port);
		return (M)this;
	}

	public java.lang.String getPort() {
		return get("port");
	}

	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}

	public java.lang.Integer getStatus() {
		return get("status");
	}

	public M setDescribe(java.lang.String describe) {
		set("describe", describe);
		return (M)this;
	}

	public java.lang.String getDescribe() {
		return get("describe");
	}

}
