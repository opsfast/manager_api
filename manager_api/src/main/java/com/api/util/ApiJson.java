package com.api.util;

import java.util.HashMap;
import java.util.Map;

public class ApiJson {
	private Boolean isSuccess = true;
	private String code = "000";
	private String message = "操作成功";
	private Object data;
	private Map<String, Object> dataMap = new HashMap<String, Object>();
	public Boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Map<String, Object> getDataMap() {
		return dataMap;
	}
	
	public void putDataMap(String key,Object data) {
		dataMap.put(key, data);
	}
}
