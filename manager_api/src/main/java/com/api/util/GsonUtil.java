package com.api.util;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken; 

public class GsonUtil {
	 private GsonUtil(){}    
     
	    /**   
	     * 对象转换成json字符串   
	     * @param obj    
	     * @return    
	     */    
	    public static String toJson(Object obj) {    
	        try{  
	              Gson gson = new Gson();    
	              return gson.toJson(obj);    
	        }catch(Exception e){  
	            e.printStackTrace();  
	              
	        }  
	        return null;  
	    }    
	      
	    /**   
	     * json字符串转成对象   
	     * @param str     
	     * @param type   
	     * @return    
	     */    
	    public static Object fromJsonToObject(String str, Type type) {    
	        try{
	             Gson gson = new Gson();    
	             return gson.fromJson(str, type);    
	        }catch(Exception e){  
	            e.printStackTrace();  
	        }  
	       return null;  
	    }    
	    
	    /**   
	     * json字符串转成对象   
	     * @param str     
	     * @param type    
	     * @return    
	     */    
	    @SuppressWarnings({ "unchecked", "rawtypes" })
		public static Object fromJsonToObject(String str, Class type) {    
	        try{  
	              Gson gson = new Gson();    
	              return gson.fromJson(str, type);    
	        }catch(Exception e){  
	            e.printStackTrace();  
	        }  
	        return null;  
	    }    
	    /**  
	     * json转List  
	     * */  
	    @SuppressWarnings("unused")
		private void test(){  
	        String json = "";  
	        String result = new Gson().fromJson(json,new TypeToken<List<Object>>() {}.getType());//转换为集合   
	    }  
}
