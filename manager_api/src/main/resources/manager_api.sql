/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : manager_api

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-09-27 16:21:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `protocol` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `requestMethod` varchar(255) DEFAULT NULL COMMENT '请求方式 get post',
  `requestBody` text,
  `responseBody` text,
  `module_id` int(11) DEFAULT NULL,
  `create_time` varchar(20) DEFAULT NULL,
  `disposeType` varchar(255) DEFAULT NULL COMMENT 'base：普通的请求      des：请求des加密,响应des解密  ',
  `status` int(1) DEFAULT NULL COMMENT '1有效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of api
-- ----------------------------
INSERT INTO `api` VALUES ('22', '登陆', 'http://', '127.0.0.1', '8080', 'manager_api/user/doLogin', 'post', null, '{\"code\":\"000\",\"data\":null,\"dataMap\":{},\"message\":\"登陆成功\",\"isSuccess\":true}', '2', null, 'base', '1');
INSERT INTO `api` VALUES ('26', '测试des', null, null, null, null, null, null, null, '2', null, null, '0');

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of config
-- ----------------------------

-- ----------------------------
-- Table structure for domain
-- ----------------------------
DROP TABLE IF EXISTS `domain`;
CREATE TABLE `domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ipOrDomain` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of domain
-- ----------------------------

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `orderNum` int(10) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL COMMENT '1有效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES ('2', '用户模块', '15', '1', '1');

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectName` varchar(255) DEFAULT NULL COMMENT '项目名称',
  `protocol` varchar(255) DEFAULT NULL COMMENT 'http  https',
  `ip` varchar(255) DEFAULT NULL,
  `port` varchar(20) DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '1有效',
  `describe` varchar(255) DEFAULT NULL COMMENT '项目描述 简介',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='项目表';

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('1', '项目', 'http://', '127.0.0.1', '8080', '1', '项目简介');
INSERT INTO `project` VALUES ('9', null, null, null, null, '0', null);

-- ----------------------------
-- Table structure for requestheader
-- ----------------------------
DROP TABLE IF EXISTS `requestheader`;
CREATE TABLE `requestheader` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `api_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of requestheader
-- ----------------------------

-- ----------------------------
-- Table structure for requestparams
-- ----------------------------
DROP TABLE IF EXISTS `requestparams`;
CREATE TABLE `requestparams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL COMMENT '值',
  `notes` varchar(255) DEFAULT NULL COMMENT '注释',
  `example` varchar(255) DEFAULT NULL,
  `is_must` smallint(255) DEFAULT NULL COMMENT '参数 是否是 必须   1必须 0非必须',
  `type` varchar(255) DEFAULT NULL COMMENT '参数类型  text file',
  `api_id` int(11) DEFAULT NULL COMMENT '对应的接口Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1272 DEFAULT CHARSET=utf8 COMMENT='参数表';

-- ----------------------------
-- Records of requestparams
-- ----------------------------
INSERT INTO `requestparams` VALUES ('1270', 'name', 'admin', '用户名', null, '1', null, '22');
INSERT INTO `requestparams` VALUES ('1271', 'pwd', '123456', '密码', null, '1', null, '22');

-- ----------------------------
-- Table structure for responseparams
-- ----------------------------
DROP TABLE IF EXISTS `responseparams`;
CREATE TABLE `responseparams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL COMMENT '值',
  `notes` varchar(255) DEFAULT NULL COMMENT '注释',
  `type` varchar(255) DEFAULT NULL COMMENT '参数类型  text file',
  `api_id` int(11) DEFAULT NULL COMMENT '对应的接口Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='参数表';

-- ----------------------------
-- Records of responseparams
-- ----------------------------
INSERT INTO `responseparams` VALUES ('37', 'message', '登陆成功', '返回消息', null, '22');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `val` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'is_pay', '1', '前台是否显示支持作者（1显示 0隐藏）', null);
INSERT INTO `sys_config` VALUES ('2', 'registerCode', 'HelloWorld', '注册用到的注册码', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('5', 'admin', 'e10adc3949ba59abbe56e057f20f883e');
