manager_api项目是基于java语言开发的项目。其主要目的是帮助开发者、测试人员测试接口的bug。
项目采用jfinal+layerUi+arttemplate+freemarker+mysql开发。

有什么不懂的一起加个群大家有时间一起聊会
<a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=97f1b0f6f636df40cceadad80a177d0439746b6d8f1ecf8ff1d428639ae61ba3"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="manager_api群" title="manager_api群">点我</a>


项目部署只需要改config的数据库链接即可

![config路径](https://git.oschina.net/uploads/images/2017/0927/150656_b553a3e0_535567.png "屏幕截图.png")

亲 楼主用的是tomcat启动的喔

账号：admin
密码：123456

功能简介

    登陆：只是为了防止其他用户乱点瞎按。

    项目管理：更好的区分接口属于哪个项目，不至于混淆。

    添加模块：这个项目分为几大块（举个栗子：用户模块、支付模块、积分模块等等）

    添加接口：这个模块有哪些接口
        
    小工具：
        
        1：域名组功能（添加几个域名方面快速切换方便快速访问测试、开发、正式环境的域名）
        2：批量请求功能（方便测试例如 登陆密码次数输入过多 导致封号5分钟、购买商品的时候可以方便测试下商品数量是否 超购了）
        
![工具截图](https://git.oschina.net/uploads/images/2017/0927/150148_58e6126a_535567.png "屏幕截图.png")

编辑删除模块和接口的时候鼠标右键
![编辑删除模块和接口的时候鼠标右键](https://git.oschina.net/uploads/images/2017/0703/220053_66c05ee6_535567.png "在这里输入图片标题")

   配置表（需要注意的）：
   ![输入图片说明](https://git.oschina.net/uploads/images/2017/0703/212616_f64059e4_535567.png "sys_config表截图")
    
    如果你们前面有个nginx访问路径不对的话可修改这里（看下图）
![输入图片说明](https://git.oschina.net/uploads/images/2017/0927/152436_aeea3cd0_535567.png "屏幕截图.png")

方便用户二次开发自己的加密方式：
集成BaseDispose类里面提供封装好的一些方法
```
package com.api.dispose.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.api.model.Requestparams;
import com.api.util.DesUtil;
import com.api.util.GsonUtil;
import com.jfinal.kit.StrKit;

public class DesDispose extends BaseDispose{
	
	/**
	 *  我这边就举个栗子
	 *  每个公司 用到的加密方式都不一样
	 *  继承BaseDispose 就是方便调用 disposeRequest()
	 */
	
	/**
	 *	把参数加密
	 */
	public  Map<String, String> disposeRequest(List<Requestparams> list){
		Map<String, String> temp_map = super.disposeRequest(list);
		Map<String, String> map = new HashMap<String, String>();
		if(map != null){
			map.put("data", DesUtil.encode(GsonUtil.toJson(temp_map)));
		}
		return map;
	}
	//des解密
	public  String disposeResponse(String responseStr){
		if(StrKit.notBlank(responseStr)){
			responseStr = DesUtil.decode(responseStr);
		}
		return responseStr;
	}
}

```
```
        public void doRequest(){
		ApiJson json = new ApiJson();
		String allUrl = ApiUtil.getAllUrl(getRequest());
		//参数验证
		if(ApiUtil.isBank_allUrl(getRequest())){
			json.setIsSuccess(false);
			json.setMessage("ip url 不能为空");
			renderJson(json);
			return;
		}
		//用到了那个加密方式就直接加if  自己处理下自己的数据的加解密
		BaseDispose baseDispose = null;
		if("base".equals(getPara("disposeType"))){
			baseDispose = new BaseDispose();
		}else if("des".equals(getPara("disposeType"))){
			 baseDispose = new DesDispose();
		}else{
			baseDispose = new BaseDispose();
		}
		
		
		
		Map<String, String> queryParas = baseDispose.disposeRequest(getManagerApi_requestParams());
		HttpJson httpJson = ApiUtil.httpRequest(allUrl, queryParas, getPara("requestMethod"));
		if(!httpJson.getIsSuccess()){
			json.setIsSuccess(false);
			json.setMessage("错误信息是："+httpJson.getData());
			renderJson(json);
			return;
		}
		//处理返回值
		String responseStr = baseDispose.disposeResponse(httpJson.getData());
		json.setData(responseStr);
		json.setIsSuccess(true);
		json.setMessage("请求成功");
		renderJson(json);
	}
```


登陆进去看到的那个背景图了吗？

那是我16年过年回家拍的老家的蓝天白云。

以此做背景祝大家代码干净无bug。
```
System.out.println("Hello World  ԅ(¯ㅂ¯ԅ)");
```


















    